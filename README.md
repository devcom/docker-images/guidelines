# Guidelines

Developer guidelines and standards for all Docker base image projects. Extends the guidelines in [devcom/guidelines](https://code.vt.edu/devcom/guidelines).